# Restore Lambda Commons

This project bundles some common python functions for rds mysql restore lambdas.

## release new version

Just tag the main branch with something like `v0.5.4` or for a development version `v0.5.4.dev`. The rest
should be handled by the ci pipeline.

For a python versioning guide check
https://packaging.python.org/en/latest/discussions/versioning/#choosing-a-versioning-scheme 

## dependencies

Even though boto3 and urllib3 are already available on lambda, they are probably outdated. so this is still part
of the requirements

Any other only for CI or development needed depencies are defined separately.

You can install packages from `requirements.dev.txt` to get those.

Our own package mysql-user-provisioner is only optional and is not part of the requirements. Please install it if
needed.