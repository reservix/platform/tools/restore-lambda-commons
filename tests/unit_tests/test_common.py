from restore_lambda_commons import common

import unittest


class TestCommonInitHandler(unittest.TestCase):

    def test_inithandler_simple(self):
        event, aws_client = common.init_handler({"replace_db_cluster_id": "test"}, {})
        self.assertEqual(event["replace_db_cluster_id"], "test")

    def test_dtorhandler_simple(self):
        event = common.dtor_handler({}, "previous", {})
        self.assertEqual(event["previous_stages"], ["previous"])


if __name__ == '__main__':
    unittest.main()
