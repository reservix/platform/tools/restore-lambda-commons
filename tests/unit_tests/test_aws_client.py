import os

import boto3

from moto import mock_aws
from restore_lambda_commons.aws_client import AwsClient

CURRENT_CLUSTER_NAME = "cluster-stage"


def prepare_rds_cluster():
    rds_client = boto3.client('rds', region_name='eu-central-1')
    db_cluster = rds_client.create_db_cluster(
        DBClusterIdentifier=CURRENT_CLUSTER_NAME,
        AllocatedStorage=10,
        Engine="aurora-mysql",
        MasterUsername="root",
        MasterUserPassword="stub123456",
        Port=1234,
        VpcSecurityGroupIds=["sg-123456"],
        EnableCloudwatchLogsExports=["audit", "error"],
        Tags=[{"Key": "Environment", "Value": "Prod"}]
    )
    db_cluster = rds_client.create_db_cluster(
        DBClusterIdentifier=f"temp-{CURRENT_CLUSTER_NAME}",
        AllocatedStorage=10,
        Engine="aurora-mysql",
        MasterUsername="root",
        MasterUserPassword="stub123456",
        Port=1234,
        VpcSecurityGroupIds=["sg-123456"],
        EnableCloudwatchLogsExports=["audit", "error"],
    )
    rds_client.create_db_instance(
        DBClusterIdentifier=CURRENT_CLUSTER_NAME,
        DBInstanceIdentifier=f'{CURRENT_CLUSTER_NAME}-1',
        DBInstanceClass='db.r6g.large',
        Engine='aurora-mysql',
        Tags=[{"Key": "Environment", "Value": "Prod"}],
    )

    os.environ["AWS_DEFAULT_REGION"] = "eu-central-1"


@mock_aws
def test_get_current_db_cluster():
    prepare_rds_cluster()

    aws_client = AwsClient(CURRENT_CLUSTER_NAME)
    assert aws_client.get_current_db_cluster()["DBClusterIdentifier"] == CURRENT_CLUSTER_NAME


@mock_aws
def test_product_env_title_case():
    prepare_rds_cluster()

    my_aws_client = AwsClient(CURRENT_CLUSTER_NAME)
    my_aws_client.attach_instances("temp", "stage")

    rds_client = boto3.client('rds', region_name='eu-central-1')
    clusters = rds_client.describe_db_clusters()

    assert clusters["DBClusters"][1]["TagList"] == [{"Key": "Environment", "Value": "Stage"}]
