import os

import logging
import hvac

logger = logging.getLogger("handler")


class Vault:
    _client = None
    _vault_header_value = None
    _vault_aws_role = None

    def __init__(self):
        raise RuntimeError('Call client() instead')

    @classmethod
    def client(cls, vault_addr=None, vault_header_value=None, vault_aws_role=None):
        if cls._client is None:
            cls._client = hvac.Client(url=vault_addr)
            cls._vault_header_value = vault_header_value
            cls._vault_aws_role = vault_aws_role
        cls._authenticate()

        return cls._client

    @classmethod
    def _authenticate(cls):
        if cls._client.is_authenticated():
            return
        on_lambda = 'AWS_LAMBDA_FUNCTION_NAME' in os.environ
        if on_lambda:
            cls._client.auth.aws.iam_login(os.environ['AWS_ACCESS_KEY_ID'],
                                           os.environ['AWS_SECRET_ACCESS_KEY'],
                                           os.environ['AWS_SESSION_TOKEN'],
                                           header_value=cls._vault_header_value,
                                           role=cls._vault_aws_role,
                                           use_token=True,
                                           region=os.environ["AWS_REGION"])
        if not cls._client.is_authenticated():
            RuntimeError("Vault can't authenticate")

    @classmethod
    def get_kv2_secret_version(cls, secret_path, version=None):
        split_path = secret_path.split("/")
        secret_path = "/".join(split_path[2:])
        mount_point = split_path[0]
        return cls.client().secrets.kv.v2.read_secret_version(secret_path, version=version, mount_point=mount_point)
