import os

from mysql_user_provisioner import Provisioner
from jinja2 import Environment, FileSystemLoader, select_autoescape
from restore_lambda_commons.vault import Vault


class UserProvisioner:
    def __init__(self, product_env, connection_env, override_environment_variables=None):
        """
        Create a UserProvisioner object

        :product_env str The environment the lambda is running in, probably "stage"
        :connection_env str The environment the mysql connection secrets get rendered in, probably "prod"
        :vault_environment dict An optional dict to override environment variables in the template renderer
        """
        self.product_env = product_env
        self.connection_env = connection_env
        self.override_environment_variables = override_environment_variables
        self.config = self._load_template().render()

    def start_provisioning(self):
        provisioner = Provisioner(config_string=self.config)
        provisioner.provision_users()

    def _load_template(self, template_filename="config_template.yml"):
        env = Environment(
            loader=FileSystemLoader(""),
            autoescape=select_autoescape()
        )
        env.filters["hashi_vault"] = self._vault_filter
        env.filters["environment"] = self._environment_filter

        return env.get_template(template_filename)

    def _vault_filter(self, path):
        path = path.format(env=self.product_env, connection_env=self.connection_env).split(":")
        secret = Vault.get_kv2_secret_version(path[0])
        return secret['data']['data'][path[1]]

    def _environment_filter(self, var):
        if var in self.override_environment_variables:
            return self.override_environment_variables[var]

        return "" if var not in os.environ else os.environ[var]
