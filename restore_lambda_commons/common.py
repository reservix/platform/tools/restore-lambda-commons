import logging
import os

from restore_lambda_commons.aws_client import AwsClient

log_level = logging.INFO if "LOG_LEVEL" not in os.environ else getattr(logging, os.environ["LOG_LEVEL"])

if len(logging.getLogger().handlers) > 0:
    # The Lambda environment pre-configures a handler logging to stderr. If a handler is already configured,
    # `.basicConfig` does not execute. Thus we set the level directly.
    logging.getLogger().setLevel(log_level)
else:
    logging.basicConfig(level=log_level)
logger = logging.getLogger("handler")


def init_handler(event, context):
    logger.info("Event received:")
    logger.info(event)

    if "stop_after_stages" in event and event["stop_after_stages"] is not None:
        if event["stop_after_stages"] <= 0:
            logger.info("stop_after_stages is 0 or lower, exiting")
            exit()
        else:
            event["stop_after_stages"] -= 1

    return event, AwsClient(event["replace_db_cluster_id"])


def dtor_handler(event, stage_name, additional_info=None):
    additional_info = f" Additional stage info: {additional_info}" if additional_info is not None else ""
    logger.info(f"{stage_name} successfully finished.{additional_info}")

    if "previous_stages" in event and event["previous_stages"] is not None:
        event["previous_stages"].append(stage_name)
    else:
        event["previous_stages"] = [stage_name]

    return event
