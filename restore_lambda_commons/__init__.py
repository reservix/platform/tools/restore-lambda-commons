import os

__version__ = os.environ["PACKAGE_VERSION"].strip("v") if "PACKAGE_VERSION" in os.environ else "0.0.0.dev"

