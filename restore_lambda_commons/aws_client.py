import logging
import os

import boto3
import json

logger = logging.getLogger("aws_client.py")


class AwsClient:
    def __init__(self, replace_db_cluster_id=None):
        self.replace_db_cluster_id = replace_db_cluster_id
        self.session = self._get_aws_session()
        self._rds_client = None
        self._backup_client = None
        self._lambda_client = None

    @staticmethod
    def _get_aws_session():
        if "AWS_PROFILE" in os.environ:
            return boto3.Session(profile_name=os.environ["AWS_PROFILE"])
        else:
            return boto3.Session()

    @property
    def rds_client(self):
        if not self._rds_client:
            self._rds_client = self.session.client('rds')
        return self._rds_client

    @property
    def backup_client(self):
        if not self._backup_client:
            self._backup_client = self.session.client('backup')
        return self._backup_client

    @property
    def lambda_client(self):
        if not self._lambda_client:
            self._lambda_client = self.session.client('lambda')
        return self._lambda_client

    def add_environment_tag(self, resource_arn, product_env):
        return self.rds_client.add_tags_to_resource(ResourceName=resource_arn,
                                                    Tags=[{"Key": "Environment", "Value": product_env}])

    def get_current_db_cluster(self):
        return self.get_db_cluster(self.replace_db_cluster_id)

    def get_db_cluster(self, db_cluster_id):
        cluster_list = self.rds_client.describe_db_clusters(Filters=[
            {
                'Name': 'db-cluster-id',
                'Values': [
                    db_cluster_id,
                ]
            },
        ])['DBClusters']
        if len(cluster_list) <= 0:
            raise Exception("No DB clusters found")
        return cluster_list[0]

    def _get_db_cluster_by_prefix(self, cluster_db_prefix):
        db_clusters = self.rds_client.describe_db_clusters()['DBClusters']
        for cluster in db_clusters:
            if cluster["DBClusterIdentifier"].startswith(cluster_db_prefix.lower()):
                return cluster

    def _get_current_db_instances(self, db_cluster_id=None):
        if db_cluster_id is None:
            cluster = self.get_current_db_cluster()
        else:
            cluster = self.get_db_cluster(db_cluster_id)

        return self.rds_client.describe_db_instances(
            Filters=[
                {
                    'Name': 'db-cluster-id',
                    'Values': [
                        cluster["DBClusterIdentifier"],
                    ]
                },
            ],
        )["DBInstances"]

    def start_restore_job(self, recovery_point_arn, iam_role_arn, temp_db_cluster_prefix,
                          idempotency_token=None):
        db_cluster = self.get_current_db_cluster()
        db_cluster["DBClusterIdentifier"] = f"{temp_db_cluster_prefix}-{db_cluster['DBClusterIdentifier']}"
        metadata = {
            "AvailabilityZones": json.dumps(db_cluster["AvailabilityZones"]),
            "CopyTagsToSnapshot": json.dumps(db_cluster.get("CopyTagsToSnapshot")),
            "DBClusterIdentifier": db_cluster["DBClusterIdentifier"],
            "DBClusterParameterGroupName": db_cluster["DBClusterParameterGroup"],
            "DBSubnetGroupName": db_cluster["DBSubnetGroup"],
            "EnableCloudwatchLogsExports": json.dumps(db_cluster["EnabledCloudwatchLogsExports"]),
            "EnableIAMDatabaseAuthentication": json.dumps(db_cluster["IAMDatabaseAuthenticationEnabled"]),
            "Engine": db_cluster["Engine"],
            "EngineVersion": db_cluster["EngineVersion"],
            "EngineMode": db_cluster["EngineMode"],
            "KmsKeyId": db_cluster["KmsKeyId"],
            "Port": json.dumps(db_cluster["Port"]),
            "VpcSecurityGroupIds": json.dumps([sg["VpcSecurityGroupId"] for sg in db_cluster["VpcSecurityGroups"]]),
        }
        if db_cluster.get("BacktrackWindow"):
            metadata["BacktrackWindow"] = db_cluster.get("BacktrackWindow")
        if db_cluster.get("ScalingConfiguration"):
            metadata["ScalingConfiguration"] = json.dumps(db_cluster.get("ScalingConfiguration"))
        if db_cluster.get("ServerlessV2ScalingConfiguration"):
            metadata["ServerlessV2ScalingConfiguration"] = json.dumps(db_cluster["ServerlessV2ScalingConfiguration"])
        if idempotency_token is not None:
            return self.backup_client.start_restore_job(
                RecoveryPointArn=recovery_point_arn,
                Metadata=metadata,
                IamRoleArn=iam_role_arn,
                IdempotencyToken=idempotency_token,
                ResourceType='Aurora'
            )
        else:
            return self.backup_client.start_restore_job(
                RecoveryPointArn=recovery_point_arn,
                Metadata=metadata,
                IamRoleArn=iam_role_arn,
                ResourceType='Aurora'
            )

    def start_backup_job(
            self,
            backup_vault_name,
            resource_arn,
            iam_role_arn,
            lifecycle,
            idempotency_token=None):
        return self.backup_client.start_backup_job(BackupVaultName=backup_vault_name, ResourceArn=resource_arn,
                                                   IamRoleArn=iam_role_arn, Lifecycle=lifecycle,
                                                   IdempotencyToken=idempotency_token)["BackupJobId"]

    def start_copy_job(
            self,
            recovery_point_arn: str,
            source_backup_vault_name: str,
            destination_backup_vault_arn: str,
            iam_role_arn: str,
            idempotency_token: str,
            lifecycle: dict) -> str:
        return self.backup_client.start_copy_job(
            RecoveryPointArn=recovery_point_arn,
            SourceBackupVaultName=source_backup_vault_name,
            DestinationBackupVaultArn=destination_backup_vault_arn,
            IamRoleArn=iam_role_arn,
            IdempotencyToken=idempotency_token,
            Lifecycle=lifecycle)['CopyJobId']

    def attach_instances(self, temp_db_cluster_prefix, product_env, max_instances=None,
                         feature_serverless_conversion=False, ignore_autoscaling_instances=False):
        db_cluster = self.get_current_db_cluster()
        db_cluster_instances = self._get_current_db_instances(db_cluster_id=db_cluster['DBClusterIdentifier'])
        new_db_cluster_identifier = f"{temp_db_cluster_prefix}-{db_cluster['DBClusterIdentifier']}"[:62]

        # fix Environment tags
        account_id = self.session.client('sts').get_caller_identity().get('Account')
        cluster_arn = f"arn:aws:rds:{self.session.region_name}:{account_id}:cluster:{new_db_cluster_identifier}"

        environment_tags = [tag for tag in db_cluster["TagList"] if tag.get("Key") == "Environment"]
        product_env_tag = product_env.title() if len(environment_tags) > 0 and environment_tags[
            0]["Key"].istitle() else product_env
        self.add_environment_tag(cluster_arn, product_env_tag)

        instances_created = []
        features = {
            "serverless": {
                "enabled": False,
                "instances": {}
            }
        }

        if db_cluster.get("ServerlessV2ScalingConfiguration"):
            logger.info(
                f"Setting ServerlessV2ScalingConfiguration to: {str(db_cluster['ServerlessV2ScalingConfiguration'])}")
            self.rds_client.modify_db_cluster(
                DBClusterIdentifier=new_db_cluster_identifier,
                ServerlessV2ScalingConfiguration=db_cluster["ServerlessV2ScalingConfiguration"]
            )

        for instance in db_cluster_instances:
            new_instance_identifier = f"{temp_db_cluster_prefix}-{instance['DBInstanceIdentifier']}"[:63]
            if max_instances is not None and len(instances_created) >= max_instances:
                break
            if feature_serverless_conversion and instance["DBInstanceClass"] == "db.serverless":
                features["serverless"]["enabled"] = True
                features["serverless"]["instances"][new_instance_identifier] = {
                    "DBInstanceClass": instance["DBInstanceClass"],
                }
                instance["DBInstanceClass"] = "db.r6g.large"
            if ignore_autoscaling_instances and "application-autoscaling" in instance['DBInstanceIdentifier']:
                continue

            kwargs = {
                "DBClusterIdentifier": new_db_cluster_identifier,
                "DBInstanceIdentifier": new_instance_identifier,
                "DBInstanceClass": instance["DBInstanceClass"],
                "Engine": instance["Engine"],
                "DBSecurityGroups": instance["DBSecurityGroups"],
                "AvailabilityZone": instance["AvailabilityZone"],
                "PreferredMaintenanceWindow": instance["PreferredMaintenanceWindow"],
                "DBParameterGroupName": instance["DBParameterGroups"][0]["DBParameterGroupName"],
                "MultiAZ": instance["MultiAZ"],
                "AutoMinorVersionUpgrade": instance["AutoMinorVersionUpgrade"],
                "PubliclyAccessible": instance["PubliclyAccessible"],
                "CopyTagsToSnapshot": instance["CopyTagsToSnapshot"],
            }
            if "OptionGroupName" in instance["OptionGroupMemberships"][0]:
                kwargs["OptionGroupName"]: instance["OptionGroupMemberships"][0]["OptionGroupName"]
            if "DBSubnetGroup" in instance and "DBSubnetGroupName" in instance["DBSubnetGroup"]:
                kwargs["DBSubnetGroupName"] = instance["DBSubnetGroup"]["DBSubnetGroupName"]
            if "MonitoringRoleArn" in instance and "MonitoringInterval" in instance:
                kwargs["MonitoringInterval"] = instance["MonitoringInterval"]
                kwargs["MonitoringRoleArn"] = instance["MonitoringRoleArn"]
            else:
                logger.info(f"Enhanced monitoring not configured for {kwargs['DBInstanceIdentifier']}")

            if "PerformanceInsightsEnabled" in instance and instance["PerformanceInsightsEnabled"]:
                kwargs["EnablePerformanceInsights"] = instance["PerformanceInsightsEnabled"]
                kwargs["PerformanceInsightsKMSKeyId"] = instance["PerformanceInsightsKMSKeyId"]
                kwargs["PerformanceInsightsRetentionPeriod"] = instance["PerformanceInsightsRetentionPeriod"]
            else:
                logger.info(f"Performance insights not configured for {kwargs['DBInstanceIdentifier']}")

            tags = []
            for tag in instance["TagList"]:
                if tag["Key"] == "Environment":
                    tag["Value"] = product_env_tag
                tags.append(tag)
            kwargs["Tags"] = tags
            instances_created.append(self.rds_client.create_db_instance(**kwargs)["DBInstance"])

        return features, instances_created

    def get_db_instances(self, db_instance_identifier):
        return self.rds_client.describe_db_instances(
            DBInstanceIdentifier=db_instance_identifier
        )["DBInstances"]

    def delete_cluster(self, db_cluster_id, skip_final_snapshot):
        self.rds_client.modify_db_cluster(
            DBClusterIdentifier=db_cluster_id,
            DeletionProtection=False,
        )
        deleted_resources = []
        for instance in self._get_current_db_instances(db_cluster_id=db_cluster_id):
            deleted_resources.append(self.rds_client.delete_db_instance(
                DBInstanceIdentifier=instance["DBInstanceIdentifier"],
                SkipFinalSnapshot=skip_final_snapshot,
                DeleteAutomatedBackups=True
            )["DBInstance"]["DBInstanceIdentifier"])

        deleted_resources.append(self.rds_client.delete_db_cluster(
            DBClusterIdentifier=db_cluster_id,
            SkipFinalSnapshot=skip_final_snapshot,
        )["DBCluster"]["DBClusterIdentifier"])
        return deleted_resources

    def rename_cluster(self, before_cluster_id, after_cluster_id):
        for instance in self._get_current_db_instances(db_cluster_id=before_cluster_id):
            before_instance_identifier = instance["DBInstanceIdentifier"]
            after_instance_identifier = instance["DBInstanceIdentifier"].replace(before_cluster_id,
                                                                                 after_cluster_id, 1)
            self.rds_client.modify_db_instance(DBInstanceIdentifier=before_instance_identifier,
                                               NewDBInstanceIdentifier=after_instance_identifier,
                                               ApplyImmediately=True)

        self.rds_client.modify_db_cluster(
            DBClusterIdentifier=before_cluster_id,
            NewDBClusterIdentifier=after_cluster_id,
            ApplyImmediately=True
        )

    def use_serverless_instances(self, serverless_instances, temp_db_cluster_prefix):
        new_db_cluster = self._get_db_cluster_by_prefix(cluster_db_prefix=temp_db_cluster_prefix)
        new_db_cluster_instances = self._get_current_db_instances(db_cluster_id=new_db_cluster['DBClusterIdentifier'])

        modified_instances = []
        for instance in new_db_cluster_instances:
            self.rds_client.modify_db_instance(
                DBInstanceIdentifier=instance["DBInstanceIdentifier"],
                DBInstanceClass=serverless_instances[instance["DBInstanceIdentifier"]]["DBInstanceClass"],
                ApplyImmediately=True,
            )
            modified_instances.append(instance["DBInstanceIdentifier"])
        return modified_instances

    def invoke_lambda(self, function_name, payload):
        prefix = "restore-stage"
        self.lambda_client.invoke(
            FunctionName=f"{prefix}-{function_name}",
            InvocationType='Event',
            Payload=json.dumps(payload)
        )

    def describe_restore_job_status_from_restore_job_id(self, job_id) -> dict:
        restore_job = self.backup_client.describe_restore_job(RestoreJobId=job_id)
        return {
            "CreatedResourceArn": "Unknown" if "CreatedResourceArn" not in restore_job else restore_job[
                "CreatedResourceArn"],
            "Status": "Unknown" if "Status" not in restore_job else restore_job["Status"],
            "StatusMessage": "Unknown" if "StatusMessage" not in restore_job else restore_job["StatusMessage"],
            "PercentDone": "0" if "PercentDone" not in restore_job else restore_job["PercentDone"],
        }

    def describe_backup_job_status_from_backup_job_id(self, job_id) -> dict:
        backup_job = self.backup_client.describe_backup_job(BackupJobId=job_id)
        return {
            "RecoveryPointArn": "Unknown" if "RecoveryPointArn" not in backup_job else backup_job["RecoveryPointArn"],
            "BackupVaultArn": "Unknown" if "BackupVaultArn" not in backup_job else backup_job["BackupVaultArn"],
            "BackupVaultName": "Unknown" if "BackupVaultName" not in backup_job else backup_job["BackupVaultName"],
            "Status": "Unknown" if "State" not in backup_job else backup_job["State"],
            "StatusMessage": "Unknown" if "StatusMessage" not in backup_job else backup_job["StatusMessage"],
            "PercentDone": "0" if "PercentDone" not in backup_job else backup_job["PercentDone"]
        }
