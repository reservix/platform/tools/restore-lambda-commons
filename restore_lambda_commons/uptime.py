import urllib3
import json


def send_uptime_heartbeat(url):
    http = urllib3.PoolManager()
    http.request('POST',
                 url,
                 body=json.dumps({"response_time": 0.8}),
                 headers={'Content-Type': 'application/json'})
